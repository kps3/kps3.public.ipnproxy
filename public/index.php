<?php
  $config = require __DIR__ . '/config.php';

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    /**
     * Post means the request is coming from the IPN Service.
     */
    $agent = !empty($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'No User Agent!';
    //@TODO check User Agent

    $data = [
      'user_agent' => $agent,
      'ip'         => !empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : 'No Remote Address!',
      'timestamp'  => date('Y-m-d H:i:s'),
      'post'       => $_POST
    ];

    file_put_contents($config['storage_path'] . '/' . microtime(true) . '.json', json_encode($data));

    print 'OK';
  } elseif ($_SERVER['REQUEST_METHOD'] == 'GET') {
    /**
     * Get means it is coming from the local service, requesting IPNs
     */
    $files = glob($config['storage_path'] . '/*.json');
    $payload = [];
    for ($x = 0, $c = count($files); $x < $c; $x++) {
      $payload[] = file_get_contents($files[$x]);
    }
    header('Content-Type: application/json');
    print json_encode($payload);

    if (empty($_GET['view'])) {
      // delete files
      for ($x = 0, $c = count($files); $x < $c; $x++) {
        @unlink($files[$x]);
      }
    }
  }