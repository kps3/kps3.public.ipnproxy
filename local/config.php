<?php
  return [
    // Where to pull the data, this is the same url that should be used as the IPN Post back.
    'proxyUrl' => '',
    // Where to post the data that is received from the proxy.
    'postUrl'  => '',
    // Number of seconds between polling requests.
    'sleep'    => 10
  ];