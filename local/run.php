<?php
  $config = require __DIR__ . '/config.php';
  $fs = fopen(__DIR__ . '/logs/' . date('Y-m-d') . '.log', 'a');
  $sleep = (!empty($config['sleep']) && intval($config['sleep'], 10) > 0) ? intval($config['sleep'], 10) : 10;
  print "IPN Proxy Polling Started with {$sleep} second delay." . PHP_EOL;
  while (true) {
    $response = json_decode(file_get_contents($config['proxyUrl']), true);
    for ($x = 0, $c = count($response); $x < $c; $x++) {
      $data = json_decode($response[$x], true);
      if (!$data) continue;
      $post = http_build_query($data['post']);
      $length = strlen($post);
      $userAgent = $data['user_agent'];
      $ip = $data['ip'];
      print "Found POST From {$userAgent} ({$ip}) : {$post}" . PHP_EOL;;
      $context = stream_context_create(
        [
          'http' => [
            'method'  => 'POST',
            'header'  => "Content-type: application/x-www-form-urlencoded\r\nUser-Agent: {$userAgent}\r\nContent-Length: {$length}\r\n",
            'content' => $post
          ]
        ]);
      $postResponse = file_get_contents($config['postUrl'], null, $context);
      fputcsv(
        $fs, [
        date('H:i:s'),
        $post,
        $postResponse
      ]);
    }
    sleep($sleep);
  }
  fclose($fs);
 