## IPN Proxy

Postback Proxy for Amazon IPN. Would probably work with other postback services, but only tested with Amazon IPN.  Don't
use this on a production system, it is designed for testing IPN postbacks in development.

1. Everything in /public should be put on a server that is accessible to the entire Internet.  This is where Amazon
  IP postbacks will be sent.

2. Make sure web server can write to /public/files.  This is where the postback requests are stored.

3. Setup your Amazon FPS calls to use the URL where you installed the IPN Proxy.  This can be done at an Amazon account
level or using the OverrideIPNUrl parameter in requests.  See the Amazon documentation for more details on this.

4. On your local machine, open up /local/config.php and change the 'proxyUrl' and 'postUrl' configuration parameters.
'proxyUrl' is the URL where you installed the /public scripts in the steps above.  'postUrl' is where you want the post
to be made, this could be your local machine or some other host that is internal and not accessible to the outside
 Internet (if it was, then why would you be using this?).

5. In the /local directory run "php run.php", you will see output from the posts in STDOUT, these posts are also sent
to the 'postUrl' that is configured.

Copyright © 2014 KPS3

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.